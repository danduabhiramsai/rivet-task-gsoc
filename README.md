# Matrix Inversion Project

## Introduction
So in this task I demonstrated the creation and inversion of an NxN random matrix using Python and NumPy,also I created a Docker container and automating the process through GitLab CI/CD. So the goal is to measure the execution time required for matrix inversion with N=5000, exploring the efficiencies of numerical computations with NumPy.

## Objectives
- To practice matrix manipulation using the NumPy library.
- To learn Docker containerization for Python applications.
- To automate the build and execution of the Docker container using GitLab CI/CD.
- To analyze the performance of matrix inversion for a large matrix size (N=5000).

## Technologies Used
- **Python**: The core programming language used for the project.
- **NumPy**: A powerful library for numerical computations in Python.
- **Docker**: Used for creating a containerized environment for the project.
- **GitLab CI/CD**: For automating the build and execution process.

## Project Structure
- `matrix_inversion.py`: The Python script responsible for generating an NxN random matrix and computing its inverse.
- `Dockerfile`: Defines the steps to build the Docker image, including setting up the Python environment and installing NumPy.
- `.gitlab-ci.yml`: Contains the CI/CD pipeline configuration for automating the Docker image build and measuring execution time.

## Setup and Running Instructions
1. **Clone the repository**: 
git clone https://gitlab.com/danduabhiramsai/rivet-task-gsoc
2. **Build the Docker image**:
docker build -t matrix-inversion .
3. **Run the Docker container**:
docker run matrix-inversion 5000

Replace `5000` with any value of N to test different matrix sizes.

## Challenges Faced and Solutions
While building the project I faced few issues while configuring the GitLab CI/CD pipeline to correctly build the Docker image and measure execution time. This was addressed by carefully setting up the `.gitlab-ci.yml` file and ensuring the Dockerfile was optimized for quick builds and efficient execution.

## Results and Observations
The project successfully demonstrated that NumPy could efficiently handle matrix inversion tasks, even for large matrices (N=5000), within a containerized environment. The execution time measurement provided insights into the computational complexity and performance considerations.

## Multi-Architecture Build
To create a multi-architecture Docker build system that works both locally and via CI/CD, we leveraged Docker Buildx for its support of building images for amd64 and arm64 platforms from a single command. This ensures our application is ready for a wide range of devices and servers.

**Tools Used**:

*Docker Buildx*: For multi-architecture builds.

*GitLab CI/CD*: For automating the build and deployment process.

**Command Used**:

`docker buildx build --platform linux/amd64,linux/arm64 -t uweeds/matrixx:latest . --push`

**Implementation**:

*Dockerfile*: Ensured compatibility with both amd64 and arm64.
*Local Testing*: Used Docker Buildx commands to build and test images locally.
*CI/CD Pipeline*: Configured .gitlab-ci.yml to automate multi-architecture builds with Buildx and push images to a registry.

**Challenges**:

Lack of arm64 runners in GitLab CI/CD limited our ability to directly test on arm64 through CI/CD.
Proceeded with amd64 builds and planned for future arm64 testing alternatives.

## Self-hosted macOS Runners Setup

Due to the lack of available macOS runners in GitLab CI/CD, we leverage self-hosted macOS runners to ensure our project remains compatible with macOS environments. This brief guide outlines the steps to set up and use a macOS machine as a CI/CD runner.

**Quick Setup Guide**

*Preparation*:

 Choose a macOS machine to act as your runner. Ensure it's stable and connected to the internet.

*Installation*:

*Install GitLab Runner using Homebrew*: brew install gitlab-runner

*Start the GitLab Runner service*: gitlab-runner start
Registration:

*Register your runner with your project*: sudo gitlab-runner register

You'll need your GitLab instance URL and the registration token from your project's CI/CD settings.
Assign descriptive tags (e.g., macos, arm64) during registration to help target CI/CD jobs specifically to this runner.
Configuration: Use the tags assigned during registration in your .gitlab-ci.yml to direct specific jobs to run on your macOS runner.



