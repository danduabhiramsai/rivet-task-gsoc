FROM python:3.9-slim

WORKDIR /app

COPY matrix_inversion.py .
COPY requirements.txt .

RUN pip install --no-cache-dir -r requirements.txt

ENTRYPOINT ["python", "matrix_inversion.py"]
