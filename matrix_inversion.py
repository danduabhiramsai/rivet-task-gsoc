import numpy as np
import sys

def create_and_invert_matrix(n):
    matrix = np.random.rand(n, n)
    print("Original Matrix:")
    print(matrix)

    try:
        inv_matrix = np.linalg.inv(matrix)
        print("Inverted Matrix:")
        print(inv_matrix)
    except np.linalg.LinAlgError:
        print("The matrix is singular and cannot be inverted.")

if __name__ == "__main__":
    if len(sys.argv) > 1:
        N = int(sys.argv[1])
    else:
        N = 5
    create_and_invert_matrix(N)
